<?php
include 'config.php';
include 'database.php';
include 'functions.php';
require 'vendor/autoload.php';

// Обрабатываем пришедшие данные
// По параметру action определяем, какую функцию вызвать
if (!empty($_REQUEST) && ($_REQUEST['ajax'] && $_REQUEST['ajax'] == 1)) {
    header("Content-Type: application/json, text/html; charset={$conf['main']['charset']}");
    switch ($_REQUEST['action']) {
        case 'save_data': {
            $params = $_REQUEST;
            if (array_key_exists('file', $_FILES)){
                if ($_FILES['file']['error'][0] === UPLOAD_ERR_OK) {
                    $params['files'] = reArrayFiles($_FILES['file']);
                } else {
                    echo json_encode(['type' => 'danger', 'message' => "При загрузке файлов произошла ошибка: ".$_FILES['file']['error'][0]]);
                }
            } else $params['files'] = [];
            echo save_data($db, $conf, $params);
            break;
        }
        case 'get_data': {
            echo get_data($db);
            break;
        }
    }
}