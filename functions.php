<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

function getInstagramData() {
    // Был создан новый аккаунт в Instagram, который зарегестрирован для работы с api.
    $client_id = '54859ca61cb940e3bc7c6366ff2d4082';
    $access_token = '11418409874.54859ca.605ef7900b3342bc8e11f8b9a674fdb6';
    $user_id = '5696899354'; // user_id аккаутнта с которого будем тянуть посты можно найти в ссылке ~/user_name/?__a=1 из пришедшего JSON
    $dev_user_id = '11418409874'; // ID аккаунта, от имени которого шлем запрос.
    // HTTP-заголовки, без которых instagram просто вернет пустую строку
    $headers = [
//        'content-type: application/json; charset=utf-8',
//        'x-requested-with: XMLHttpRequest',
        "cookie: ds_user_id={$dev_user_id}; sessionid={$dev_user_id}%3Azp5AdiWMNaNBVh%3A24;"
    ];

    // URL API, который возвращает последние фото
//    $result = file_get_contents('https://api.instagram.com/v1/users/'.$user_id.'/media/recent/?client_id='.$client_id.'&access_token='.$access_token.'&count=6');
    $instagram_curl = curl_init(); // инициализация cURL подключения
//    curl_setopt($instagram_curl, CURLOPT_URL, "https://api.instagram.com/v1/users/{$user_id}/media/recent?access_token={$access_token}");
    curl_setopt($instagram_curl, CURLOPT_URL, "https://www.instagram.com/webstripe/?__a=1");
    curl_setopt($instagram_curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($instagram_curl, CURLOPT_HTTPHEADER, $headers);
    $result = curl_exec($instagram_curl);
    curl_close($instagram_curl);
    $result = json_decode($result, true);
    $result = $result['graphql']['user']['edge_owner_to_timeline_media']['edges'];

    if (!empty($result)) {
        // Собираем полученные данные в массив с необходимыми свойствами
        $return_data = [];
        $count = 6;
        foreach ($result as $row) {
            if ($count>0) {
                // Берем только фото
                if (!$row['node']['is_video']) {
                    $return_data[] = [
                        'image' => $row['node']['thumbnail_resources'][3]['src'],
                        'description' => $row['node']['edge_media_to_caption']['edges'][0]['node']['text'],
                        'likes' => $row['node']['edge_liked_by']['count']
                    ];
                    $count--;
                }
            }
        }
    }

    return $return_data;
}

function save_data ($db, $conf, $original_data = []) {
//    print_r(var_dump($original_data));
    $message = ""; $type = '';

    if (!empty($original_data)) {
        $save_data = [
            'name' => $original_data['name'],
            'email' => $original_data['email'],
            'text' => $original_data['text'],
            'value1' => $original_data['value1'],
            'value2' => $original_data['value2'],
            'value3' => $original_data['value3']
        ];

        // сохранение в БД
        $result = mysql_get_data($db, 'data');
        if (!$result) {
            $result = mysql_insert($db, 'data', $save_data);
        } else {
            $result = mysql_update($db, 'data', $save_data);
        }
        if ($result[0] == 'success') {
            $message = $message."Данные сохранены в базе данных.<br/>"; $type = 'success';
        } else if ($result[0] == 'error') {
            $message = $message."При сохранении данных в базу данных произошла ошибка:</br>{$result[1]}.<br/>";
            $type = 'danger';
        }

        // Сохранение файлов
        $files = (!empty($original_data['files'])) ? save_file($original_data['files']) : [];
        if ($files && !empty($files)) {
            $message = $message."Файлы успешно были сохранены на сервер: <br/>";
            $type = 'success';
        } else {
            $message = $message."При сохранении файлов произошла ошибка.<br/>"; $type = 'danger';
        }

        // Генерация excel
        $xls = generate_xls($original_data);
        if ($xls) {
            $message = $message."Файл Excel сгенерирован. <br/>"; $type = 'success';
        } else {
            $message = $message."При создании файла Excel произошла ошибка. <br/>"; $type = 'danger';
        }

        // Отправка email
        $send_result = generate_email([
            'target' => $original_data['email'],
            'subject' => "Письмо от {$conf['main']['site_name']}",
            'message' => "Любой текст.",
            'attachments' => array_merge((array)$xls, (array)$files)
        ]);
        if ($send_result[0] == 'success') {
            $message = $message."Письмо было отправлено.<br/>"; $type = 'success';
        } else {
            $message = $message."При отправке письма произошла ошибка: <br/>".$send_result[1];
            $type = 'danger';
        }

        return json_encode(['type' => $type, 'message' => $message]);
    } else return json_encode(['type' => 'danger', 'message' => "<strong>ERROR: </strong>Отсутсвуют данные для обработки."]);
}

function get_data ($db) {
    $result = mysql_get_data($db, 'data');

    if (!$result) {
        $return_data = ['type' => 'danger', 'message' => "Данные отсутсвуют.<br/>"];
    } else {
        $return_data = ['type' => 'success', 'data' => $result[0]];
    }

    return json_encode($return_data);
}

// Данная функция была взята с http://php.net/manual/ru/features.file-upload.multiple.php
// для упрощения жизни программиста
function reArrayFiles(&$file_post) {
    $file_ary = array();
    $file_count = count($file_post['name']);
    $file_keys = array_keys($file_post);

    for ($i=0; $i<$file_count; $i++) {
        foreach ($file_keys as $key) {
            $file_ary[$i][$key] = $file_post[$key][$i];
        }
    }

    return $file_ary;
}

function mysql_get_data($db, $table_name, $condition = '') {
    $sql = "SELECT * FROM {$table_name}";
    if (!empty($condition)) {
        $sql .= "WHERE {$condition}";
    }

    if ($result = $db->query($sql)) {
        $result_arr = [];
        while ($row = $result->fetch_assoc()) {
            $result_arr[] = $row;
        }

        return $result_arr;
    } else return false;
}

function mysql_insert($db, $table_name, $data) {
    $sql = "INSERT INTO {$table_name} (";

    $count = count($data);

    $l = $count;
    foreach ($data as $k => $v) {
        $sql .= $k;
        $l--;
        if ($l > 0) $sql .= ", ";
    }
    $sql .= ") VALUES (";

    $l = $count;
    foreach ($data as $k => $v) {
        $sql .= "'".$db->real_escape_string($v)."'";
        $l--;
        if ($l > 0) $sql .= ", ";
    }
    $sql .= ")";

    if ($db->query($sql)) return ['success', true]; else return ['error', $db->error];
}

function mysql_update($db, $table_name, $data) {
    $sql = "UPDATE {$table_name} SET ";

    $l = count($data);
    foreach ($data as $k => $v) {
        $sql .= "{$k} = '".$db->real_escape_string($v)."'";
        $l--;
        if ($l > 0) $sql .= ", ";
    }

    $result = $db->query($sql);

    if ($result != 0) return ['success', $result]; else return ['error', $db->error];
}

function save_file($files) {
    $return_data = [];
    $error = false;

    foreach ($files as $file) {
        $suffix = md5(file_get_contents($file['tmp_name']));
        $info = pathinfo($file['name']);
        $ext = $info['extension'];
        $file['name'] = "attachfile_{$suffix}.{$ext}";
        $filename = "uploads/".$file['name'];
        if (move_uploaded_file($file['tmp_name'], $filename)) {
            $return_data[] = $filename;
        } else $error = true;
    }

    if (!$error) return $return_data; else return false;
}

function generate_xls ($params) {
    $filename = "xls/attachfile_".date('jnYGi').".xls";

    // Шапка
    $xls = "
        <!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
        <html xmlns='http://www.w3.org/1999/xhtml' xml:lang='en' lang='en'>
        <head>
            <meta http-equiv='content-type' content='text/html; charset=utf-8' />
            <meta name='author' content='oz1245' />
                <title>Прикрепленный файл Excel</title>
        </head>
        <body>";

    // Таблица c данными
    $xls .= "
        <table>
            <tr>
                <th>Имя</th>
                <td>{$params['name']}</td>
            </tr>
            <tr>
                <th>Email</th>
                <td>{$params['email']}</td>
            </tr>
            <tr>
                <th>Текстовая строка</th>
                <td>{$params['text']}</td>
            </tr>
            <tr>
                <th>Число 1</th>
                <td>{$params['value1']}</td>
            </tr>
            <tr>
                <th>Число 2</th>
                <td>{$params['value2']}</td>
            </tr>
            <tr>
                <th>Число 3</th>
                <td>{$params['value3']}</td>
            </tr>
        </table>";

    // Закрытие разметки
    $xls .= "</body></html>";

    if (file_put_contents($filename, $xls)) return $filename; else return false;
}

/**
 * @param $params:
 * from_email string email автора письма
 * from_name string имя автора письма
 * target string целевой email
 * subject string тема письма
 * message string содержание
 * attachments array прикрепленные файлы
 */
function generate_email($params) {
    global $conf;
    $from_email = (isset($params['from_email'])) ? $params['from_email'] : $conf['main']['email'];
    $from_name = (isset($params['from_name'])) ? $params['from_name'] : $conf['main']['site_name'];
    $target = (isset($params['target'])) ? $params['target'] : '';
    $subject = (isset($params['subject'])) ? $params['subject'] : 'No subject';
    $message = (isset($params['message'])) ? $params['message'] : '';
    $attachments = (isset($params['attachments'])) ? $params['attachments'] : [];

    $mail = new PHPMailer();
    $mail->setFrom(mb_convert_encoding($from_email, $conf['main']['charset']), $from_name);
    $mail->addAddress($target);

    if (!empty($attachments)) {
        foreach ($attachments as $attachment){
            $mail->addAttachment($_SERVER['DOCUMENT_ROOT']."/".$attachment);
        }
    }

    $mail->CharSet = $conf['main']['charset'];
    $mail->Subject = $subject;
    $mail->Body = $message;

    if ($mail->send()) {
        return ['success', true];
    } else {
        return ['error', $mail->ErrorInfo];
    }
}