<?php
    $conf = [
        'main' => [
            'site_name' => 'webstripett',
            'url' => $_SERVER['SERVER_NAME'],
            'title' => 'Тестовое задание Webstripe',
            'email' => 'mail@webstripett.loc',
            'charset' => 'UTF-8'
        ],
        'db' => [
            'host' => 'localhost',
            'user' => 'root',
            'password' => '',
            'database' => 'wstt',
            'port' => 3306,
            'charset' => 'utf8'
        ]
    ];