$(document).ready(function(){
    // Подключение и настройка карусели
    var carousel = $('#carousel').slick({
        dots: false,
        infinite: true,
        speed: 600,
        autoplay: true,
        autoplaySpeed: 3000,
        slidesToShow: 4,
        slidesToScroll: 1,
        adaptiveHeight: true,
        centerMode: true,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    autoplay: false
                }
            }
        ],
        prevArrow: "<div class='carousel__arrows arrow_prev h-100 justify-content-center align-items-center'><i class='material-icons'>keyboard_arrow_left</i></div>",
        nextArrow: "<div class='carousel__arrows arrow_next h-100 justify-content-center align-items-center'><i class='material-icons'>keyboard_arrow_right</i></div>"
    });

    // $("[data-toggle='tooltip']").tooltip();

    // Анимация наезда текста к посту с Instagram
    $('#carousel .carousel__item').mouseover(function () {
        instagramDescription($(this), 'open');
    });
    $('#carousel .carousel__item').mouseleave(function () {
        instagramDescription($(this), 'close');
    });

    // Обработка данных формы
    $('button#form-send').on('click', function () {
        processoringForm($('form'));
    });

    // Показать результаты
    $('button#show-result').on('click', function() {
        showResult($('#result'));
    });

    // Загрузить карту
    ymaps.ready(initMap);
});

function instagramDescription (item, action) {
    var originalHeight = $(item).find('.carousel__likes').height();
    var autoHeight = originalHeight + $(item).find('.carousel__description').height();
    if (action == 'open') {
        $(item).find('.carousel__info').animate({
            height: autoHeight
        }, 'fast');
    } else if (action == 'close') {
        $(item).find('.carousel__info').animate({
            height: originalHeight
        }, 'fast');
    }
}

function processoringForm (form) {
    var params = [];

    // Создаем удобный массив для валидации данных
    $(form).find('input').each(function(){
        if ($(this).attr('name') !== 'file[]') {
            params.push({
                name: $(this).attr('name'),
                value: $(this).val(),
                required: $(this).prop('required')
            });
        }
    });

    validateForm(form, params);
}

function validateForm(form, params) {
    clearForm(form);

    var errors = [];
    var vars = [];

    // Проверяем каждое поле
    $.each(params, function(i, el){
        if (el.required) {
            var message = '';
            if (el.value == '') {
                message = "Поле обязательно для заполнения.";
            } else if (el.name == 'email') {
                if (!validateEmail(el.value)) message = "Неправильный e-mail.";
            } else if (el.name == 'value1' || el.name == 'value2') {
                var v = parseInt(el.value, 10);
                if (isNaN(v)) {
                    message = "Значение должно быть целым числом.";
                } else {
                    vars.push(v);
                }
            }

            // Если поле не прошло валидацию,
            // то добавить это поле в массив с проблемными полями
            if (message !== '') {
                errors.push({
                    field: el.name,
                    message: message
                });
            }
        }
    });

    // если имеются проблемные поля,
    // то выделить эти поля
    if (errors.length > 0) {
        $.each(errors, function(i, el){
            $(form)
                .find("input[name='"+el.field+"']")
                .addClass('is-invalid')
                .after("<div class='invalid-feedback'>"+el.message+"</div>");
        });
    } else {
        // Если все ок, то отчищаем форму,..
        clearForm(form);
        // суммируем числа и добавляем сумму в массив для отправки,..
        var val3 = additionVars(parseInt(vars, 10));
        if (val3) params.push({name: 'value3',  value: val3});
        // и отправляем (в функцию)
        sendForm(form, params);
    }
}

// Отчистка формы
function clearForm(form) {
    $(form)
        .find('input').removeClass('is-invalid')
        .siblings('.invalid-feedback').remove();
}

// Валидация E-mail адреса
function validateEmail(email) {
    var regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regex.test(email);
}

// Сложение чисел
function additionVars(vars) {
    // Проверяем, что нам пришел массив...
    if (Array.isArray(vars)) {
        var result = 0;
        $.each(vars, function(i, v){
            // с целыми числами
            if (typeof (v) == 'number') {
                // И суммируем их
                result += v;
            } else {
                console.error('additionVars(): Variables must be of type number.');
                return false;
            }
        });
        return result;
    } else {
        console.error('additionVars(): The argument must be of type array.');
        return false;
    }
}

// Функция отправки данных на сервер
function sendForm(form, params) {
    // Создаем объект FormData для отправки смешанных данных
    var formData = new FormData();
    var inputFile = $(form).find("input[name='file[\\]']")[0];
    if (typeof (inputFile.files) != 'undefined') {
        $.each($(form).find("input[name='file[\\]']")[0].files, function (i, file) {
            formData.append('file[]', file);
        });
    }
    $.each(params, function(i, el){
        formData.append(el.name, el.value);
    });

    // Отправляем на сервер
    $.ajax({
        url: '/ajax.php?action=save_data&ajax=1',
        type: 'POST',
        contentType: false,
        processData: false,
        dataType: 'json',
        data: formData,
        beforeSend: function() {
            // После отправки, перед получением ответа от сервера
            // запустим анимированный лоадер на форме
            loader($('form > fieldset'), true);
        },
        success: function(data) {
            var options = {
                message: data.message
            };

            // Проверяем ответ...
            if (data.type == 'success') {
                options.type = 'success';
            } else if (data.type == 'fail') {
                options.type = 'danger';
            }
            // и выводим попап с сообщением от сервера
            var popup = showAlertPopup(options);
            setTimeout(function(){
                $(popup).modal('hide');
            }, 6000);
        },
        complete: function() {
            // После получения ответа убираем лоадер
            loader($('form > fieldset'), false);
        }
    })
}

// Генерация попап с сообщением
function showAlertPopup (opt) {
    // Попап генерится на основе Bootstrap Modal
    var popup = $('<div/>', {
        class: 'modal popup_alert popup_alert__'+opt.type,
        tabindex: -1,
        role: 'dialog',
        html: $('<div/>', {
            class: 'modal-dialog modal-dialog-centered',
            role: 'document',
            html: $('<div/>', {
                class: 'modal-content',
                html: $('<div/>', {
                    class: 'modal-body',
                    html: opt.message
                })
            })
        })
    });

    $(popup).modal('show');

    return $(popup);
}

// Вывод данных
function showResult(target) {
    // Получаем данные для отображения
    $.ajax({
        url: '/ajax.php?action=get_data&ajax=1',
        method: 'POST',
        dataType: 'json',
        beforeSend: function() {
            // После запроса заблокируем кнопку
            $('button#show-result').prop('disabled', true);
        },
        success: function(data) {
            // Удалим прошлые результаты и проверим ответ
            $(target).empty();
            if (data.type == 'success') {
                // Если данные есть, то создим таблицу с данными
                var table = $('<table/>', {
                    class: 'table',
                    html: $('<tr/>', {html: [
                        $('<th/>', {text: 'Поле'}),
                        $('<th/>', {text: 'Значение'}),
                        ]})
                });
                $.each(data.data, function (key, val) {
                    $('<tr/>', {
                        html: [
                            $('<td/>', {
                                text: key
                            }),
                            $('<td/>', {
                                text: val
                            })
                        ]
                    }).appendTo(table);
                });
                $(target).append(table);

                // Работа с массивами
                showTextArray($('#result1'), [
                    data.data['name'],
                    data.data['email'],
                    data.data['text']
                ]);
                showIntArray($('#result2'), [
                    parseInt(data.data['value1'],10),
                    parseInt(data.data['value2'], 10),
                    parseInt(data.data['value3'], 10)
                ]);
            } else if (data.type == 'danger') {
                // Если данных нет, то вывести попап с сообщением
                var popup = showAlertPopup({message: data.message});
                setTimeout(function(){
                    $(popup).modal('hide');
                }, 6000);
            }
        },
        complete: function () {
            // После получения данных разблокировать кнопку
            $('button#show-result').prop('disabled', false);
        }
    });
}

// Лоадер
function loader(target, turnon) {
    if (turnon) {
        $(target)
            .prop('disabled', true)
            .parent().css('position', 'relative');
        $(target).append("<div class='loader'></div>");
    } else {
        $(target)
            .prop('disabled', false)
            .parent().css('position', 'inherit')
            .find('.loader').remove();
    }
}

// Решила почему-то разделить работу с массивами на две части
function showTextArray(target, original_data) {
    var orig_string = original_data.join('');

    // Размер первого элемента массива словами
    var new_length = parseNumber(original_data[0].length);
    var new_sring = '';
    var regex = /^([a-zа-яё]+|\d+)$/i; // Только буквы и цифры

    // Веборка каждой второй буквы с каждого элемента массива
    for (var i = 1; i < orig_string.length; i+=2) {
        if (regex.test(orig_string[i])) {
            new_sring += orig_string[i];
        } else {
            // Если пробел или символ, то заменить на "_"
            new_sring += "_";
        }
    }

    var show_data = new_sring+"<br/>"+new_length;
    $(target).html(show_data);
}

function showIntArray(target, original_data) {
    // Сумма элементов массива
    var sum = additionVars(original_data);
    var sum_string = sum.toString();

    var show_data = '';
    // Вычисление количества цифр в сумме и разбиение суммы на строки
    for (var i = 0; i < sum_string.length; i++) {
        show_data += sum_string[i]+"<br/>";
    }

    $(target).html(show_data);
}

// Функция перевода числа в строку
var parseNumber = function numberToStr(){
    var dictionary = [
        [ "", "один", "два", "три", "четыре", "пять", "шесть", "семь", "восемь", "девять",
            "десять", "одиннадцать", "двенадцать", "тринадцать", "четырнадцать", "пятнадцать",
            "шестнадцать", "семнадцать", "восемнадцать", "девятнадцать" ],
        [ "", "десять", "двадцать", "тридцать", "сорок", "пятьдесят", "шестьдесят", "семьдесят", "восемьдесят", "девяносто" ],
        [ "", "сто", "двести", "триста", "четыреста", "пятьсот", "шестьсот", "семьсот", "восемьсот", "девятьсот" ],
        [ "тысяч|а|и|", "миллион||а|ов", "миллиард||а|ов", "триллион||а|ов" ]
];
    function getNumber(number, limit){
        var temp = number.match(/^\d{1,3}([,|\s]\d{3})+/);
        if(temp) return temp[0].replace(/[,|\s]/g, "");
        temp = Math.abs( parseInt(number) );
        if( temp !== temp || temp > limit ) return null;
        return String(temp);
    }
    function setEnding(variants, number){
        variants = variants.split("|");
        number = number.charAt( number.length - 2 ) === "1" ? null : number.charAt( number.length - 1 );
        switch(number){
            case "1":
                return variants[0] + variants[1];
            case "2": case "3": case "4":
                return variants[0] + variants[2];
            default:
                return variants[0] + variants[3];
        }
    }
    function getPostfix(postfix, number){
        if( typeof postfix === "string" || postfix instanceof String ){
            if( postfix.split("|").length < 3 ) return " " + postfix;
            return " " + setEnding(postfix, number);
        }
        return "";
    }

    return function(number, postfix){
        if(typeof number === "undefined")
        return "999" + new Array(dictionary[3].length + 1).join(" 999");
        number = String( number );
        var minus = false;
        number.replace(/^\s+/, "").replace(/^-\s*/, function(){
            minus = true;
            return "";
        });
        number = getNumber(number, Number( new Array(dictionary[3].length + 2).join("999") ));
        if(!number) return "";
        postfix = getPostfix(postfix, number);
        if(number === "0") return "ноль" + postfix;
        var position = number.length, i = 0, j = 0, result = [];
        while(position--){
            result.unshift( dictionary[ i++ ][ number.charAt(position) ] );
            if(i === 2 && number.charAt(position) === "1" )
            result.splice(0, 2, dictionary[0][ number.substring( position, position + 2 ) ]);
            if(i === 3 && position !== 0 ){
                i = 0;
                if( position > 3 && number.substring( position - 3, position ) === "000" ){
                    j++; continue;
                }
                result.unshift( setEnding(dictionary[3][j++], number.substring( 0, position )) );
            }
        }
        position = result.length - 5;
        switch( result[position] ){
            case "один": result[position] = "одна"; break;
            case "два": result[position] = "две"; break;
        }
        if(minus) result.unshift("минус");
        return result.join(" ").replace(/\s+$/, "").replace(/\s+/g, " ") + postfix;
    };
}();

function initMap(){
    // Создание карты
    var myMap = new ymaps.Map("map", {
        // Координаты центра карты
        center: [56.473461, 84.968673],
        // Уровень масштабирования
        zoom: 16
    });

    // Балун с информацией компании
    var myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
        balloonContentHeader: 'Webstripe'
    });
    myMap.geoObjects.add(myPlacemark);
}