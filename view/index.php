<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?=$conf['main']['title']?></title>

    <!--    jQuery  -->
    <script type="text/javascript" src="/node_modules/jquery/dist/jquery.min.js"></script>

    <!--    Bootstrap 4 -->
    <link rel="stylesheet" type="text/css" href="/node_modules/bootstrap/dist/css/bootstrap.min.css">
    <script type="text/javascript" src="/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="/node_modules/popper.js/dist/umd/popper.min.js"></script>

    <!--    Slick Carousel  -->
    <link rel="stylesheet" type="text/css" href="/node_modules/slick-carousel/slick/slick.css">
    <link rel="stylesheet" type="text/css" href="/node_modules/slick-carousel/slick/slick-theme.css">
    <script type="text/javascript" src="/node_modules/slick-carousel/slick/slick.min.js"></script>

    <!--    Material Design Icons   -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!--    Yandex Map API  -->
    <script src="https://api-maps.yandex.ru/2.1/?apikey=373cd0a6-b6fa-4d14-ac92-0f48981c78ee&lang=ru_RU" type="text/javascript"></script>

    <!--    Main    -->
    <link rel="stylesheet" type="text/css" href="/css/style.css">
    <script type="text/javascript" src="/js/script.js"></script>
</head>
<body>

<div class="container-fluid">

    <section id="section-carousel" class="section row">
        <div class="col-md p-0">
            <? $carousel_data = getInstagramData(); ?>
            <? if (!empty($carousel_data)) : ?>
                <div id="carousel" class="section__carousel carousel">
                    <? foreach ($carousel_data as $item) : ?>
                        <div class="col-md-3 carousel__item">
                            <img src="<?=$item['image']?>" alt="Instagram post" class="carousel__picture">
                            <div class="row">
                                <div class="col-md carousel__info text-center">
                                    <div class="row">
                                        <div class="col-md carousel__likes">
                                            <i class="material-icons carousel__like">favorite</i>
                                            <strong><span class="carousel__like_count"><?=$item['likes']?></span></strong><br/>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md carousel__description">
                                            <p class="text-left"><?=$item['description']?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <? endforeach; ?>
                </div>
            <? endif; ?>
        </div>
    </section> <!-- /#section-carousel -->

    <section id="section-form" class="section row">
        <div class="col-md">
            <div class="row">
                <div class="col-md section__form">
                    <div class="row">
                        <div class="col-md">
                            <h4 class="section__title text-center">Форма</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md">
                            <form>
                                <fieldset>
                                    <div class="form-group">
                                        <input type="text" id="form-name" class="form-control" name="name" placeholder="Имя" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" id="form-email" class="form-control" name="email" placeholder="Email" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" id="form-text" class="form-control" name="text" placeholder="Текстовая строка" required>
                                    </div>
                                    <div class="row">
                                        <div class="col-md">
                                            <div class="form-group">
                                                <input type="text" id="form-value1" class="form-control" name="value1" placeholder="Число 1" required>
                                            </div>
                                        </div>
                                        <div class="col-md">
                                            <div class="form-group">
                                                <input type="text" id="form-value2" class="form-control" name="value2" placeholder="Число 2" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input type="file" id="form-file" class="form-control-file" name="file[]" multiple>
                                    </div>
                                    <div class="form-group text-center">
                                        <button id="form-send" class="btn btn-primary" type="button">Отправить</button>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md section__form_result">
                    <div class="row">
                        <div class="col-md">
                            <h4 class="section__title text-center">Результат</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md">
                            <div class="form-group text-center">
                                <button id="show-result" class="btn btn-primary" type="button">Вывести</button>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div id="result" class="col-md"></div>
                    </div>
                    <div class="row">
                        <div id="result1" class="col-md"></div>
                    </div>
                    <div class="row">
                        <div id="result2" class="col-md"></div>
                    </div>
                </div>
            </div>
        </div>
    </section> <!-- /#section-form -->

    <section id="section-map" class="section row">
        <div class="col-md p-0 section__map map">
            <div id="map" class="map__container"></div>
        </div>
    </section> <!-- /#section-map -->

</div> <!-- /.container-fluid -->

</body>
</html>