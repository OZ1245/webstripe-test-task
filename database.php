<?php
$db = new mysqli($conf['db']['host'], $conf['db']['user'], $conf['db']['password'], $conf['db']['database'], $conf['db']['port']);
if ($db->connect_errno) {
    echo "Не удалось подключиться к MySQL: " . $db->connect_error;
}

$db->set_charset($conf['db']['charset']);